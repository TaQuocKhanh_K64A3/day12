<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Validate Form</title>
	<style>
		.content1 {
			width: 50%;
			margin: 0 25%;
			border: 1.5px solid #42729e;
			padding: 25px 75px 20px 50px;
		}

		.form-row1 {
			margin: 5px 0px 5px 0px;
		}

		.form-label1 {
			display: inline-block;
			background-color: #5b9bd5;
			border: 1.5px solid #42729e;
			padding: 10px 20px 8px 8px;
			color: white;
			width: 30%;
			margin: 5px 10px 5px 0px;
			text-align: center;
		}

		.form-control1 {
			padding: 10px 10px;
		}

		.gender {
			margin: 0 5%;
		}

		.login-btn-wp {
			display: flex;
			justify-content: center;
			margin: 30px 0px 0px 0px;
		}

		.login-btn {
			background-color: #70ad47;
			border: 1.5px solid #4c749e;
			color: white;
			padding: 12px 10px;
			border-radius: 5px;
			width: 20%;
		}

		img {
			max-width: 100%;
			max-height: 100%;
		}

		#image {
			height: 10em;
		}
	</style>
</head>

<body>
	<?php
	session_start();
	$fullName = isset($_SESSION['data']['fullName']) ? $_SESSION['data']['fullName'] : "";
	$gender = isset($_SESSION['data']['gender']) ? $_SESSION['data']['gender'] : "";
	$khoa = isset($_SESSION['data']['khoa']) ? $_SESSION['data']['khoa'] : "";
	$birthday = isset($_SESSION['data']['birthday']) ? $_SESSION['data']['birthday'] : "";
	$address = isset($_SESSION['data']['address']) ? $_SESSION['data']['address'] : "";
	$image = isset($_SESSION['image']) ? $_SESSION['image'] : "";

	?>
	<div class="container1">
		<div class="content1">

			<div class="form-group1">

				<form action="" method="POST" enctype="multipart/form-data">
					<div class="form-row1" id="name">
						<div class="form-label1"><label>Họ và tên<span style="color: #ff0000">*</span></label></div>
						<?php
						echo '<span class="form-control1">' . $fullName . '</span>'
						?>
					</div>

					<div class="form-row1" id="gender">
						<div class="form-label1"><label>Giới tính<span style="color: #ff0000">*</span></label></div>
						<?php
						echo '<span class="form-control1">' . $gender . '</span>'
						?>
					</div>

					<div class="form-row1" id="khoa">
						<div class="form-label1"><label>Phân khoa<span style="color: #ff0000">*</span></label></div>
						<?php
						echo '<span class="form-control1">' . $khoa . '</span>'
						?>
					</div>

					<div class="form-row1" id="birth">
						<div class="form-label1"><label>Ngày sinh<span style="color: #ff0000">*</span></label></div>
						<?php
						echo '<span class="form-control1">' . $birthday . '</span>'
						?>
					</div>

					<div class="form-row1" id="address">
						<div class="form-label1"><label>Địa chỉ</label></div>
						<?php
						echo '<span class="form-control1"><span>' . $address . '</span>'
						?>
					</div>

					<div class="form-row1" id="image">
						<div class="form-label1"><label>Hình ảnh</label></div>
						<?php
						echo '<span class="form-control1"><img src="' . $image . '"></span>'
						?>
					</div>
					<div class="login-btn-wp"><input type="submit" name="submit-btn" class="login-btn" value="Xác nhận"></div>

				</form>
			</div>

		</div>
	</div>

	<?php
	if (!empty($_POST['submit-btn'])) {
		include 'Connect.php';
		$gender = $gender == 'Nam' ? 1 : 0;
		$khoa = $khoa == "Khoa học máy tính" ? "MAT" : "KDL";
		$birthday = explode("/", $birthday);
		$birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0] . ' 00:00:00';
		$values = "'" . $fullName . "'," . $gender . ",'" . $khoa . "','" . $birthday . "','" . $address . "','" . $image . "'";
		$query = 'insert into student(name,gender,faculty,birthday,address,avartar) values (' . $values . ');';
		$result = mysqli_query($conn, $query);
		if (!$result) {
			$message  = 'Invalid query: ' . mysqli_error($conn) . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
		}
		mysqli_close($conn);
		header("Location: ./complete_regis.php");
	}
	?>
</body>

</html>